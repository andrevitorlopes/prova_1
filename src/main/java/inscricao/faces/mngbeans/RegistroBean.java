/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

import java.io.Serializable;
import java.util.ArrayList;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import utfpr.faces.support.PageBean;

/**
 *
 * @author andre
 */

@Named
@ApplicationScoped
public class RegistroBean implements Serializable{
    private ArrayList<Logado> LogadosList = new ArrayList<>();

    public ArrayList<Logado> getLogadosList() {
        return LogadosList;
    }

    public void addLogado(Logado logado) {
        this.LogadosList.add(logado);
    }
}
