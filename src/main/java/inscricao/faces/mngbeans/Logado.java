/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import javax.inject.Named;

/**
 *
 * @author andre
 */
public class Logado implements Serializable{
    private String nome;
    private Date data;

    public Logado(String nome){
        this.nome = nome;
        this.data = new Date();
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getData() {
        return data;
    }

    public void setData() {
        this.data = new Date();
    }
    
}
