/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;

/**
 *
 * @author andre
 */
@Named
@SessionScoped
public class LoginBean implements Serializable {
    private String login;
    private String senha;
    private boolean admin;
    
    @Inject
    private RegistroBean logadosList;
    
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public RegistroBean getLogadosList() {
        return logadosList;
    }

    public void setLogadosList(RegistroBean logadosList) {
        this.logadosList = logadosList;
    }
    
    
    public String login(){
        if(this.login.equals(this.senha)){
            logadosList.addLogado(new Logado(login));
            
            if(admin){
                return "admin";
            }
            return "cadastro";
        }
        
        FacesContext ctx = FacesContext.getCurrentInstance();
        ctx.addMessage("error", new FacesMessage("Acesso negado"));
        
        return "login";
    }
    
}
